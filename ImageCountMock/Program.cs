﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using ImageCount;
using Microsoft.Extensions.Configuration;

namespace ImageCountMock
{
  class Program
  {
    private static readonly Dictionary<string, string> RpcDefaults = new Dictionary<string, string>()
    {
      {"host", "127.0.0.1"},
      {"port", "50051"}
    };
    class ImageCountImpl : ImageCountCollectionSvc.ImageCountCollectionSvcBase
    {
      public override async Task ImageCountCollection(IAsyncStreamReader<ICCRequest> requestStream,
        IServerStreamWriter<ICCReply> responseStream, ServerCallContext context)
      {
        var rand = new Random();
        Console.WriteLine($"Starting processing of stream from {context.Peer}");
        while (await requestStream.MoveNext())
        {
          var req = requestStream.Current;
          Console.WriteLine($"Received request for {req.Accession}");
          var reply = new ICCReply() {Accession = req.Accession, Count = rand.Next(0x0FFF)};
          Console.WriteLine($"Sending reply {reply}");
          await responseStream.WriteAsync(reply);
        }
        Console.WriteLine($"Request stream from {context.Peer} closed");
      }
    }

    static void Main(string[] args)
    {
      var grpcConf = new ConfigurationBuilder()
        .AddInMemoryCollection(RpcDefaults)
        .AddEnvironmentVariables("RPC_")
        .Build();
      var Port = int.Parse(grpcConf["port"]);
      Server server = new Server
      {
        Services = {ImageCountCollectionSvc.BindService(new ImageCountImpl())},
        Ports = {new ServerPort(grpcConf["host"], Port, ServerCredentials.Insecure)}
      };
      server.Start();

      Console.WriteLine("Greeter server listening on port " + Port);
      Console.WriteLine("Press any key to stop the server...");
      Console.ReadKey();

      server.ShutdownAsync().Wait();
    }
  }
}