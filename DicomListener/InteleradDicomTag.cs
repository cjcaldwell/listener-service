﻿using System;
using System.Collections.Generic;
using System.Text;
using Dicom;

namespace DicomListener
{
    class InteleradDicomTag
    {
        public static DicomTag ImageCompressionFraction { get; } = new DicomTag(0x0029, 0x01, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ImageQuality { get; } = new DicomTag(0x0029, 0x02, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ImageBytesTransferred { get; } = new DicomTag(0x0029, 0x03, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag Image_Bounding_Box { get; } = new DicomTag(0x0029, 0x08, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag Image_Bounding_Box_Algorithm_ID { get; } = new DicomTag(0x0029, 0x09, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cParameterType { get; } = new DicomTag(0x0029, 0x10, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cPixelRepresentation { get; } = new DicomTag(0x0029, 0x11, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cBitsAllocated { get; } = new DicomTag(0x0029, 0x12, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cPixelShiftValue { get; } = new DicomTag(0x0029, 0x13, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cPlanarConfiguration { get; } = new DicomTag(0x0029, 0x14, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cRescaleIntercept { get; } = new DicomTag(0x0029, 0x15, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cSmallestPixelValue { get; } = new DicomTag(0x0029, 0x16, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cLargestPixelValue { get; } = new DicomTag(0x0029, 0x17, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag PixelDataMD5SumPerFrame { get; } = new DicomTag(0x0029, 0x20, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag HistogramPercentileLabels { get; } = new DicomTag(0x0029, 0x21, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag HistogramPercentileValues { get; } = new DicomTag(0x0029, 0x22, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag VoiLutWindowLevelValueRange { get; } = new DicomTag(0x0029, 0x23, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag VoiLutRealPixelValueRange { get; } = new DicomTag(0x0029, 0x24, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag VoiLutIndex { get; } = new DicomTag(0x0029, 0x25, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag J2cRescaleSlope { get; } = new DicomTag(0x0029, 0x26, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ImageRotationAngle { get; } = new DicomTag(0x0029, 0x27, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag DisplayedAreaTLHC { get; } = new DicomTag(0x0029, 0x28, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag DisplayedAreaBRHC { get; } = new DicomTag(0x0029, 0x29, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag InteleViewer_Markup_Type { get; } = new DicomTag(0x0071, 0x01, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Text { get; } = new DicomTag(0x0071, 0x02, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Text_Offset { get; } = new DicomTag(0x0071, 0x03, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Position { get; } = new DicomTag(0x0071, 0x04, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Half_Vector { get; } = new DicomTag(0x0071, 0x05, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Vector_1 { get; } = new DicomTag(0x0071, 0x06, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Vector_2 { get; } = new DicomTag(0x0071, 0x07, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Orthogonal_Position { get; } = new DicomTag(0x0071, 0x08, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Orthogonal_Half_Vector { get; } = new DicomTag(0x0071, 0x09, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Minimum_Length { get; } = new DicomTag(0x0071, 0x0a, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Number_of_Polygons { get; } = new DicomTag(0x0071, 0x0b, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Length_of_Polygons { get; } = new DicomTag(0x0071, 0x0c, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Points { get; } = new DicomTag(0x0071, 0x0d, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Slice_Separation { get; } = new DicomTag(0x0071, 0x0e, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Volume_Of_Interest { get; } = new DicomTag(0x0071, 0x0f, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Sagittal_Text_Offset { get; } = new DicomTag(0x0071, 0x10, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Default_Text_Offset { get; } = new DicomTag(0x0071, 0x11, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Dataset_Tool_ID { get; } = new DicomTag(0x0071, 0x20, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Distance_Calibration { get; } = new DicomTag(0x0071, 0x21, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Image_Grid_Origin { get; } = new DicomTag(0x0071, 0x30, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Image_Grid_Pixel_Row_Vector { get; } = new DicomTag(0x0071, 0x31, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Image_Grid_Pixel_Column_Vector { get; } = new DicomTag(0x0071, 0x32, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InteleViewer_Markup_Image_Grid_Is_Calibrated { get; } = new DicomTag(0x0071, 0x33, "INTELERAD MEDICAL SYSTEMS INTELEVIEWER");
        public static DicomTag InstitutionCode { get; } = new DicomTag(0x3f01, 0x01, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag RoutedTransferAE { get; } = new DicomTag(0x3f01, 0x02, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag SourceAE { get; } = new DicomTag(0x3f01, 0x03, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag DeferredValidation { get; } = new DicomTag(0x3f01, 0x04, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag SeriesOwner { get; } = new DicomTag(0x3f01, 0x05, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag OrderGroupNumber { get; } = new DicomTag(0x3f01, 0x06, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag StrippedPixelData { get; } = new DicomTag(0x3f01, 0x07, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag PendingMoveRequest { get; } = new DicomTag(0x3f01, 0x08, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag DicomPremapSites { get; } = new DicomTag(0x3f01, 0x09, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag AssociationDate { get; } = new DicomTag(0x3f01, 0x0a, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag AssociationTime { get; } = new DicomTag(0x3f01, 0x0b, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag UseSourceAe { get; } = new DicomTag(0x3f01, 0x0c, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag CorrectionVersion_1 { get; } = new DicomTag(0x3f01, 0x0d, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag RelayToAe { get; } = new DicomTag(0x3f01, 0x0e, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ImsMoveRequestRedirectionCount { get; } = new DicomTag(0x3f01, 0x0f, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ImsPostFix { get; } = new DicomTag(0x3f01, 0x10, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag OriginalTagCount { get; } = new DicomTag(0x3f01, 0x11, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag AssociationEpochInSecs { get; } = new DicomTag(0x3f01, 0x12, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag OriginalTransferSyntax { get; } = new DicomTag(0x3f01, 0x13, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag OriginalCalledAeTitle { get; } = new DicomTag(0x3f01, 0x14, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag OriginalSopInstanceUid { get; } = new DicomTag(0x3f01, 0x15, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag OriginalReferencedSopInstanceUid_1 { get; } = new DicomTag(0x3f01, 0x16, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag LargeElementSequence { get; } = new DicomTag(0x3f01, 0x18, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag LargeElementTag { get; } = new DicomTag(0x3f01, 0x19, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag LargeElementValueRepresentation { get; } = new DicomTag(0x3f01, 0x1a, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag LargeElementLength { get; } = new DicomTag(0x3f01, 0x1b, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag LargeElementValue { get; } = new DicomTag(0x3f01, 0x1c, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag Attribute_History_Sequence { get; } = new DicomTag(0x3f03, 0x01, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag DateTimeValueModified { get; } = new DicomTag(0x3f03, 0x02, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ModifierIdentifier { get; } = new DicomTag(0x3f03, 0x03, "INTELERAD MEDICAL SYSTEMS");
        public static DicomTag ModifierReason { get; } = new DicomTag(0x3f03, 0x04, "INTELERAD MEDICAL SYSTEMS");

    }
}