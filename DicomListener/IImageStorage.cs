﻿using System;
using System.Collections.Generic;
using System.Text;
using Dicom;

namespace DicomListener
{
    interface IImageStorage
    {
        void Store(DicomDataset dataset);
    }
}
