﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dicom;

namespace DicomListener
{
    interface IImageProcessor
    {
        Task ProcessImage(DicomDataset dataset);
    }
}
