﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Threading.Tasks;
using Dicom;

namespace DicomListener
{
    public class ImageMetadataStore : IImageStorage
    {
        private Dictionary<string, Patient> Patients = new Dictionary<string, Patient>();
        private Dictionary<string, Order> Orders = new Dictionary<string, Order>();
        private Dictionary<string, Study> Studies = new Dictionary<string, Study>();
        private Dictionary<string, Series> Series = new Dictionary<string, Series>();

        private object PatientsLock = new object();
        private object OrdersLock = new object();
        private object StudiesLock = new object();
        private object SeriesLock = new object();
        

        public void Store(DicomDataset dicomDataset)
        {
            Console.WriteLine("Storing image...");
            var patientID = dicomDataset.Get<string>(DicomTag.PatientID);
            Console.WriteLine($"Patient ID: {patientID}");
            var accessionNumber = dicomDataset.Get<string>(DicomTag.AccessionNumber) +
                                  dicomDataset.Get(dicomDataset.GetPrivateTag(InteleradDicomTag.ImsPostFix), "");
            Console.WriteLine($"Accession : {accessionNumber}");
            var studyUID = dicomDataset.Get<string>(DicomTag.StudyInstanceUID);
            Console.WriteLine($"StudyUID: {studyUID}");
            var seriesUID = dicomDataset.Get<string>(DicomTag.SeriesInstanceUID);

            lock (PatientsLock)
            {


                if (Patients.TryGetValue(patientID, out var patient))
                {
                    Console.WriteLine("Updating patient");
                    patient.Update(dicomDataset);
                }
                else
                {
                    Console.WriteLine("Inserting patient");
                    Patients.Add(patientID, new Patient(dicomDataset));
                }
            }
            lock (OrdersLock)
            {
                if (Orders.TryGetValue(accessionNumber, out var order))
                {
                    Console.WriteLine("Updating order");
                    order.Update(dicomDataset);
                }
                else
                {
                    Console.WriteLine("Inserting order");
                    Orders.Add(accessionNumber, new Order(dicomDataset));
                }
            }
            lock (StudiesLock)
            {
                if (Studies.TryGetValue(studyUID, out var study))
                {
                    Console.WriteLine("Updating study");
                    study.Update(dicomDataset);
                }
                else
                {
                    Console.WriteLine("Inserting study");
                    Studies.Add(studyUID, new Study(dicomDataset));
                }
            }
            lock (SeriesLock)
            {
                if (Series.TryGetValue(seriesUID, out var series))
                {
                    Console.WriteLine("Updating series");
                    series.Update(dicomDataset);
                }
                else
                {
                    Console.WriteLine("Inserting series");
                    Series.Add(seriesUID, new Series(dicomDataset));
                }
            }
        }
    }
    public class Patient
    {
        public string PatientID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleNames { get; set; }
        public DateTime BirthDate { get; set; }
        public string Sex { get; set; }
        public HashSet<string> Orders { get; private set; } = new HashSet<string>();

        public Patient(DicomDataset dicomDataset)
        {
            PatientID = dicomDataset.Get<String>(DicomTag.PatientID);
            Update(dicomDataset);
        }

        public void Update(DicomDataset dicomDataset)
        {
            BirthDate = dicomDataset.Get(DicomTag.PatientBirthDate, DateTime.MinValue);
            var patientFullName = dicomDataset.Get(DicomTag.PatientName, "")
                .Split("^", 3, StringSplitOptions.RemoveEmptyEntries);
            LastName = patientFullName.Length >= 1 ? patientFullName[0] : "";
            FirstName = patientFullName.Length >= 2 ? patientFullName[1] : "";
            MiddleNames = patientFullName.Length >= 3 ? patientFullName[2] : "";
            Sex = dicomDataset.Get(DicomTag.PatientSex, "O");
            var accessionNumber = dicomDataset.Get(DicomTag.AccessionNumber, "");
            var postFixTag = dicomDataset.GetPrivateTag(InteleradDicomTag.ImsPostFix);
            accessionNumber += dicomDataset.Get(dicomDataset.GetPrivateTag(InteleradDicomTag.ImsPostFix), "");
            Orders.Add(accessionNumber);
        }
    }

    public class Order
    {
        public string AccessionNumber { get; set; }
        public string PlacerOrderNumber { get; set; }
        public HashSet<string> Studies { get; private set; } = new HashSet<string>();

        public Order(DicomDataset dicomDataset)
        {
            AccessionNumber = dicomDataset.Get<String>(DicomTag.AccessionNumber);
            Update(dicomDataset);
        }

        public void Update(DicomDataset dicomDataset)
        {
            PlacerOrderNumber = dicomDataset.Get(DicomTag.PlacerOrderNumberImagingServiceRequest, "");
            var studyUID = dicomDataset.Get<String>(DicomTag.StudyInstanceUID);
            Studies.Add(studyUID);
        }
    }

    public class Study
    {
        public string StudyInstanceUID { get; set; }
        public string StudyDate { get; set; }
        public string StudyTime { get; set; }
        public string StudyDescription { get; set; }
        public string ReferringPhysicianName { get; set; }
        public string StudyID { get; set; }
        public HashSet<string> Series { get; private set; } = new HashSet<string>() ;

        public Study(DicomDataset dicomDataset)
        {
            StudyInstanceUID = dicomDataset.Get<String>(DicomTag.StudyInstanceUID);
            Update(dicomDataset);
        }

        public void Update(DicomDataset dicomDataset)
        {
            StudyDate = dicomDataset.Get(DicomTag.StudyDate, "");
            StudyTime = dicomDataset.Get(DicomTag.StudyTime, "");
            StudyDescription = dicomDataset.Get(DicomTag.StudyDescription, "");
            ReferringPhysicianName = dicomDataset.Get(DicomTag.ReferringPhysicianName, "");
            StudyID = dicomDataset.Get(DicomTag.StudyID, "");
            var seriesUID = dicomDataset.Get<String>(DicomTag.SeriesInstanceUID);
            Series.Add(seriesUID);
        }
    }

    public class Series
    {
        public string SeriesInstanceUID { get; set; }
        public string SeriesDescription { get; set; }
        public string Modality { get; set; }
        public string BodyPart { get; set; }
        public string SeriesNumber { get; set; }
        public string SourceAe { get; set; }

        public Series(DicomDataset dicomDataset)
        {
            SeriesInstanceUID = dicomDataset.Get<string>(DicomTag.SeriesInstanceUID);
            Update(dicomDataset);
        }

        public void Update(DicomDataset dicomDataset)
        {
            SeriesDescription = dicomDataset.Get(DicomTag.SeriesDescription, "");
            Modality = dicomDataset.Get(DicomTag.Modality, "");
            BodyPart = dicomDataset.Get(DicomTag.BodyPartExamined, "");
            SeriesNumber = dicomDataset.Get(DicomTag.SeriesNumber, "");
            SourceAe = dicomDataset.Get(dicomDataset.GetPrivateTag(InteleradDicomTag.SourceAE), "");
        }
    }
}