﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dicom;
using Dicom.Log;
using Dicom.Network;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DicomListener
{
    internal static class Program
    {
        public static ServiceProvider Services;

        private static void Main(string[] args)
        {
            Services = new ServiceCollection()
                .AddSingleton<IConfiguration>(s => new ConfigurationBuilder()
                    .AddJsonFile("appSettings.json", true)
                    .AddEnvironmentVariables()
                    .Build())
                .AddSingleton<IImageStorage, ImageMetadataStore>()
                .AddTransient<IImageProcessor, StorageImageProcessor>()
                .AddTransient<IImageProcessor, DummyImageProcessor>()
                .BuildServiceProvider();

            var config = Services.GetService<IConfiguration>();
            var storage = Services.GetService<IImageStorage>();


            var dcmSrv = DicomServer.Create<DicomReceiver>(int.Parse(config.GetSection("DICOM")["port"]));
            Console.WriteLine($"Listening on port {dcmSrv.Port}");
            Console.ReadKey();
        }
    }


    public class DicomReceiver : DicomService, IDicomServiceProvider, IDicomCStoreProvider, IDicomCEchoProvider
    {
        private static readonly DicomTransferSyntax[] AcceptedTransferSyntaxes =
        {
            DicomTransferSyntax.ExplicitVRLittleEndian,
            DicomTransferSyntax.ExplicitVRBigEndian,
            DicomTransferSyntax.ImplicitVRLittleEndian
        };

        private static readonly DicomTransferSyntax[] AcceptedImageTransferSyntaxes =
        {
            // Lossless
            DicomTransferSyntax.JPEGLSLossless,
            DicomTransferSyntax.JPEG2000Lossless,
            DicomTransferSyntax.JPEGProcess14SV1,
            DicomTransferSyntax.JPEGProcess14,
            DicomTransferSyntax.RLELossless,

            // Lossy
            DicomTransferSyntax.JPEGLSNearLossless,
            DicomTransferSyntax.JPEG2000Lossy,
            DicomTransferSyntax.JPEGProcess1,
            DicomTransferSyntax.JPEGProcess2_4,

            // Uncompressed
            DicomTransferSyntax.ExplicitVRLittleEndian,
            DicomTransferSyntax.ExplicitVRBigEndian,
            DicomTransferSyntax.ImplicitVRLittleEndian
        };

        private IEnumerable<IImageProcessor> ImageProcessors{ get; }

        public DicomReceiver(INetworkStream stream, Encoding fallbackEncoding, Logger log) : base(stream,
            fallbackEncoding, log)
        {
            ImageProcessors = Program.Services.GetServices<IImageProcessor>();
        }

        public DicomCEchoResponse OnCEchoRequest(DicomCEchoRequest request)
        {
            Console.WriteLine("Received echo request");
            return new DicomCEchoResponse(request, DicomStatus.Success);
        }

        public DicomCStoreResponse OnCStoreRequest(DicomCStoreRequest request)
        {
            var dicomStatus = DicomStatus.Success;
            try
            {
                Task.WaitAll(
                    ImageProcessors
                    .Select(s => s.ProcessImage(request.Dataset))
                    .ToArray());
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception occurred processing dataset: {e}");
                dicomStatus = DicomStatus.StorageStorageOutOfResources;
            }
            var response = new DicomCStoreResponse(request, dicomStatus);
            response.Command.Add(DicomTag.AffectedSOPInstanceUID, request.SOPInstanceUID);
            return response;
        }

        public void OnCStoreRequestException(string tempFileName, Exception e)
        {
            Console.WriteLine($"CStore exception : [{e}]");
        }

        public void OnReceiveAbort(DicomAbortSource source, DicomAbortReason reason)
        {
            Console.WriteLine($"Connection aborted by [{source}] with reason [{reason.ToString()}]");
        }

        public void OnConnectionClosed(Exception exception)
        {
            Console.WriteLine($"Connection closed with exception [{exception}]");
        }

        public void OnReceiveAssociationRequest(DicomAssociation association)
        {
            Console.WriteLine(
                $"Received association request from [{association.RemoteHost}:{association.RemotePort}] - calling AE [{association.CallingAE}], called AE [{association.CalledAE}]");
            foreach (var presentationContext in association.PresentationContexts)
                if (presentationContext.AbstractSyntax == DicomUID.Verification)
                {
                    presentationContext.AcceptTransferSyntaxes(AcceptedTransferSyntaxes);
                }
                else if (presentationContext.AbstractSyntax.StorageCategory != DicomStorageCategory.None)
                {
                    presentationContext.AcceptTransferSyntaxes(AcceptedImageTransferSyntaxes);
                }
            SendAssociationAccept(association);
        }

        public void OnReceiveAssociationReleaseRequest()
        {
            Console.WriteLine($"Received association release request");
            SendAssociationReleaseResponse();
        }
    }
}