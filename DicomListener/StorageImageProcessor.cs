﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dicom;

namespace DicomListener
{
    class StorageImageProcessor : IImageProcessor
    {
        private IImageStorage storage;

        public StorageImageProcessor(IImageStorage storage)
        {
            this.storage = storage;
        }

        public Task ProcessImage(DicomDataset dataset)
        {
            return Task.Run(() => storage.Store(dataset));
        }
    }
}
