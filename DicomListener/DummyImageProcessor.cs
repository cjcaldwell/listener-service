﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Dicom;

namespace DicomListener
{
    class DummyImageProcessor : IImageProcessor
    {
        public Task ProcessImage(DicomDataset dataset)
        {
            return Task.Run(() =>
            {
                var accessionNumber = dataset.Get<string>(DicomTag.AccessionNumber);
                var patientID = dataset.Get<string>(DicomTag.PatientID);
                var sopUID = dataset.Get<string>(DicomTag.SOPInstanceUID);
                if (sopUID.EndsWith("6"))
                {
                    throw new Exception("Don't like numbers ending in 6");
                }
                Console.WriteLine($"Received image : {accessionNumber} : {patientID} : {sopUID}");
            });
        }
    }
}